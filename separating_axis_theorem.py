#!/usr/bin/python3
# Separating Axis Theorem
# This is code to tell if 2 polygons are overlap or not
# Copyright (C) 2020 Rafael Lee

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import copy
import math
import random
from matplotlib import pyplot as plt
import functools
from typing import List, Tuple
epsilon = 0.00001


class Edge:
    def __init__(self, a, b):
        if type(a) != type(Point(0.0, 0.0)) or type(b) != type(Point(0.0, 0.0)):
            raise TypeError('Edge should consist of 2 points')

        self.a = a
        self.b = b

    def __str__(self):
        return "Edge(%s, %s)" % (self.a, self.b)

    def function(self):
        x1 = self.a.x
        y1 = self.a.y
        x2 = self.b.x
        y2 = self.b.y

        # function: x = constant
        if abs(x1 - x2) < epsilon:
            return LineFunction(1.0, 0.0, -1.0*x1)
        # function: y = constant
        elif abs(y1 - y2) < epsilon:
            return LineFunction(0.0, 1.0, -1.0*y1)
        # normal function
        else:
            return LineFunction((y2 - y1) / (x2 - x1),
                                -1.0,
                                y1 - x1 * (y2 - y1) / (x2 - x1))

    def perpendicular_line_function(self, point):
        '''Calculate perpendicular_line_function across point'''
        p = point
        f = self.function()
        a = f.a
        b = f.b
        c = f.c
        # b * x - a * y - b*p.x + a * p.y

        return LineFunction(b, -a, - b*p.x + a * p.y)


class LineFunction:
    '''Line is self.a * x + self.b * y + c = 0'''

    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def is_perpendicular_to(self, other):
        if type(other) != type(LineFunction(1, 1, 1)):
            raise TypeError("Type not equals for perpendicular checking")

        if abs(other.a * self.a + other.b * self.b) > epsilon:
            return False
        else:
            return True

    def is_parallel_to(self, other):
        if type(other) != type(LineFunction(1, 1, 1)):
            raise TypeError("Type not equals for parallel checking")

        if abs(self.a * other.b - other.a * self.b) > epsilon:
            return False
        else:
            return True

    def __str__(self):
        return "%f * x + %f * y + %f = 0" % (self.a, self.b, self.c)


class Point:
    # x = 0.0
    # y = 0.0

    def __init__(self, x0: float, y0: float) -> None:
        self.x = float(x0)
        self.y = float(y0)

    @classmethod
    def distance(self, p) -> float:
        return math.sqrt(math.pow(p.x-self.x, 2)+math.pow(p.y-self.y, 2))

    def eq(self, p):
        if (abs(p.x - self.x) < epsilon) and (abs(p.y - self.y) < epsilon):
            return True

    def translate(self, v):
        return Point(self.x + v.v[0], self.y + v.v[1])

    def __str__(self):
        return "Point(%f, %f)" % (self.x, self.y)


class Vector:
    # v = [0.0, 0.0]

    def __init__(self, a, b):
        self.v = []
        self.v.append(a)
        self.v.append(b)

    def __str__(self):
        return "Vector(%f, %f)" % (self.v[0], self.v[1])

    def angle_to(self, other):
        theta_self = math.atan2(self.v[1], self.v[0])
        theta_other = math.atan2(other.v[1], other.v[0])
        # angle in range [-math.pi, math.pi)
        angle = ((theta_other - theta_self + math.pi) % (2*math.pi)) - math.pi
        return angle


class Polygon:
    '''Polygon is a list of points, the last edge vector is the vector of last point to the first'''

    # vec = []

    def __init__(self, points):
        self.vec = []
        for i in points:
            if type(i) != type(Point(1, 3)):
                raise TypeError(
                    'Elements in list should be point, type(i) = %s' % str(type(i)))
            else:
                self.vec.append(copy.deepcopy(i))

    def get_number_of_points(self):
        return len(self.vec)

    def get_point(self, index):
        return self.vec[index]

    def get_points(self) -> List[Point]:
        return self.vec

    def get_edges(self) -> List[Edge]:
        edge_count = self.get_number_of_points()
        edges = []
        for i in range(edge_count):
            edges.append(
                Edge(self.get_point(i), self.get_point((i+1) % edge_count)))
        return edges

    def get_vectors(self):
        edges = self.get_edges()
        vectors = []
        for i in edges:
            vectors.append(Vector(i.b.x-i.a.x, i.b.y-i.a.y))
        return vectors

    def get_all_x(self):
        # cnt = self.get_number_of_points()
        return [(lambda x:x.x)(x) for x in self.vec]

    def get_all_y(self):
        # cnt = self.get_number_of_points()
        return [(lambda x:x.y)(x) for x in self.vec]

    def is_convex(self):
        '''Calculate angle changing different edges, if all vectors are
rotating clockwise or counter clockwise, that means the polygon is convex
Return (True, None) if it is convex,
return (False, [concave_points]) if concave'''
        angles_delta = []
        points = self.get_points()
        vectors = self.get_vectors()

        vertex_count = self.get_number_of_points()

        for i in range(len(vectors)):
            angles_delta.append(vectors[i].angle_to(
                vectors[(i+1) % vertex_count]))

        for a in angles_delta:
            print (a)

        positives = functools.reduce(
            lambda x, y: x+(lambda p: 1 if p >= 0 else 0)(y), [0]+angles_delta)
        negatives = vertex_count-positives

        positive_index = []
        negative_index = []
        for i in range(len(vectors)):
            if angles_delta[i] >= 0:
                positive_index.append(points[(i+1) % vertex_count])
            else:
                negative_index.append(points[(i+1) % vertex_count])

        if (positives != 0) and (negatives != 0):
            if positives >= negatives:
                return (False, negative_index)
            else:
                return (False, positive_index)
        else:
            return (True, None)

    def is_convex_outdated(self):
        '''Return (True, None) if it is convex,
return (False, Point) if the point is concave point.
If 2 adjacent edges are not on the side, then their cross is the concave point.'''
        def edge_on_side(e):
            e_func = e.function()
            perpendicular_line_function = e.perpendicular_line_function(e.a)
            l = perpendicular_line_function

            ox, oy = get_projection(e.a, e_func)

            X = []
            Y = []
            points = self.get_points()
            for p in points:
                x, y = get_projection(p, l)
                X.append(x)
                Y.append(y)

            # e.a and e.b projects to the same point on l
            # as we calculated earlier, e.a will project to e.a
            x_is_min_of_X = is_min(e.a.x, X)
            x_is_max_of_X = is_max(e.a.x, X)
            y_is_min_of_Y = is_min(e.a.y, Y)
            y_is_max_of_Y = is_max(e.a.y, Y)

            # y = constant, consider x only
            if abs(l.a) < epsilon:
                if not (x_is_min_of_X or x_is_max_of_X):
                    return False
            # x = constant, consider y only
            elif abs(l.b) < epsilon:
                if not (y_is_min_of_Y or y_is_max_of_Y):
                    return False
            else:
                if (x_is_min_of_X or x_is_max_of_X) != (y_is_min_of_Y or y_is_max_of_Y):
                    raise ValueError("X and Y result are not the same")
                else:
                    if not (x_is_min_of_X or x_is_max_of_X):
                        return False
            return True

        edges = self.get_edges()
        vertex_count = len(edges)

        # if vertex_count == 3:
        #     return (True, None)
        if vertex_count < 3:
            raise ValueError("Polygon cannot have less than 3 edges")
        for i in range(vertex_count):
            if not edge_on_side(edges[i]):
                if not edge_on_side(edges[(i+1) % vertex_count]):
                    return (False, edges[i].b)
                elif not edge_on_side(edges[(i-1) % vertex_count]):
                    return (False, edges[i].a)
                else:
                    raise ValueError(
                        "Not possible to have only 1 edge not on side")

        return (True, None)

    def split(self, index0, index1):
        points0 = []
        points1 = []
        vertex_count = self.get_number_of_points()

        # if index1 < index0, swap
        index0, index1 = (lambda x, y: (x, y) if x <
                          y else (y, x))(index0, index1)
        if(index0 < index1):
            for i in range(index0, index1):
                points0.append(self.get_point(i))
            points0.append(self.get_point(index1))

            for i in range(index1, vertex_count+index0):
                points1.append(self.get_point(i % vertex_count))
            points1.append(self.get_point(
                (vertex_count+index0) % vertex_count))

        polygon0 = Polygon(points0)
        polygon1 = Polygon(points1)
        return [polygon0, polygon1]

    def get_index(self, point):
        if type(point) != type(Point(0, 0)):
            raise TypeError(
                "Type error, point is not type(Point), type is %s" % str(type(point)))
        points = self.get_points()
        for i in range(len(points)):
            if point.eq(points[i]):
                return i

    def __str__(self):
        return ", ".join([(lambda x:str(x))(x) for x in self.vec])


def split_to_multiple_convex(polygon):
    def loop(polygon_list):
        is_convex, concave_points = polygon_list[0].is_convex()
        if not is_convex:
            split_index = polygon_list[0].get_index(concave_points[0])
            vertex_count = polygon_list[0].get_number_of_points()
            left, right = polygon_list[0].split(
                split_index, (split_index+2) % vertex_count)
            left_0, left_1 = loop([left])
            right_0, right_1 = loop([right])

            if left_1 == None and right_1 == None:
                return [left_0, right_0]
            elif left_1 == None:
                return [left_0, right_0, right_1]
            elif right_1 == None:
                return [left_0, left_1, right_0]
            else:
                return [left_0, left_1, right_0, right_1]
        else:
            return [polygon_list, None]

    l = loop([polygon])
    # flatten
    return [x[0] for x in l]


class Rectangle(Polygon):
    '''Rectangle is a list of 4 points
the last edge vector is the vector of last point to the first'''

    def __init__(self, points):
        if len(points) != 4:
            raise ValueError(
                "Rectangle need to have 4 points exactly, len(points) = %d" % len(points))
        Polygon.__init__(self, points)

        edges = self.get_edges()
        f0 = edges[0].function()
        f1 = edges[1].function()
        f2 = edges[2].function()
        f3 = edges[3].function()

        if not (f0.is_perpendicular_to(f1) and f2.is_perpendicular_to(f3)):
            raise ValueError("Opposite edges are parallel")

        if not (f0.is_parallel_to(f2) and f1.is_parallel_to(f3)):
            raise ValueError("Adjacent edges are not perpendicular")


def get_projection(point, line_function):
    '''Calculate the projection of a point on a line'''
    xc = point.x
    yc = point.y

    l = line_function
    a = l.a
    b = l.b
    c = l.c

    x = -((a*c - b*b*xc + a*b*yc)/(a*a + b*b))
    y = -((b*c + a*b*xc - a*a*yc)/(a*a + b*b))

    return (x, y)


def is_polygon_overlap(polygon0, polygon1):
    is_convex0, concave_points0 = polygon0.is_convex()
    is_convex1, concave_points1 = polygon1.is_convex()
    if is_convex0 and is_convex1:
        return is_convex_polygon_overlap(polygon0, polygon1)
    elif is_convex0:
        s = split_to_multiple_convex(polygon1)
        for convex in s:
            if True == is_convex_polygon_overlap(polygon0, convex):
                return True
    elif is_convex1:
        s = split_to_multiple_convex(polygon0)
        for convex in s:
            if True == is_convex_polygon_overlap(polygon1, convex):
                return True
    else:
        s0 = split_to_multiple_convex(polygon0)
        s1 = split_to_multiple_convex(polygon1)
        for i in s0:
            for j in s1:
                if True == is_convex_polygon_overlap(i, j):
                    return True
    return False


def is_convex_polygon_overlap(polygon0, polygon1):
    '''Get all edges from 2 polygons, then calculate lines perpendicular to edges.
Get all the projection of edges in both polygon and compare if they overlap
This only works for convex.
'''
    edges0 = []

    edge_count0 = polygon0.get_number_of_points()
    for i in range(edge_count0):
        edges0.append(Edge(polygon0.get_point(i % edge_count0),
                           polygon0.get_point((i+1) % edge_count0)))

    edges1 = []
    edge_count1 = polygon1.get_number_of_points()
    for i in range(edge_count1):
        edges1.append(Edge(polygon1.get_point(i % edge_count1),
                           polygon1.get_point((i+1) % edge_count1)))

    collision_checking_lines = []
    for e in edges0 + edges1:
        # print (e)
        # get the perpendicular line cross the first point of edge
        l = e.perpendicular_line_function(e.a)
        if not l.is_perpendicular_to(e.function()):
            raise ValueError("perpendicular line calculation error")

        collision_checking_lines.append(l)

    for l in collision_checking_lines:
        projection_0_x = []
        projection_0_y = []
        for e in edges0:
            x, y = get_projection(e.a, l)
            projection_0_x.append(x)
            projection_0_y.append(y)

        projection_1_x = []
        projection_1_y = []
        for e in edges1:
            x, y = get_projection(e.a, l)
            projection_1_x.append(x)
            projection_1_y.append(y)

        x_overlap = overlap(projection_0_x, projection_1_x)
        y_overlap = overlap(projection_0_y, projection_1_y)

        # y = constant, consider x only
        if l.a < epsilon:
            if False == x_overlap:
                return False
        # x = constant, consider y only
        elif l.b < epsilon:
            if False == y_overlap:
                return False
        else:
            if x_overlap != y_overlap:
                raise ValueError("X and Y result are not the same")
            else:
                if x_overlap == False:
                    return False

    return True


def generate_polygon_from_point_list(l):
    points = []
    for i in l:
        points.append(Point(i[0], i[1]))
    return Polygon(points)


def generate_rectangle_from_point_list(l):
    points = []
    for i in l:
        points.append(Point(i[0], i[1]))
    return Rectangle(points)


def plt_plot_polygon(p, color="black", marker="o", fill=False):
    X = p.get_all_x()
    Y = p.get_all_y()
    X = X+[X[0]]
    Y = Y+[Y[0]]
    if fill:
        plt.fill(X, Y, color=color)
    else:
        plt.plot(X, Y, marker=marker, color=color)


def generate_random_rectangle(origin=None, width=None, height=None, angle=None):
    if (origin == None):
        origin = Point(random.random()*5, random.random()*5)
    if (width == None):
        width = random.random()*3
    if (height == None):
        height = random.random()*2
    if (angle == None):
        angle = random.random()*math.pi

    '''If all variables are default, all of them is random'''
    x0, y0 = origin.x, origin.y

    v1 = Vector(math.cos(angle) * width,
                math.sin(angle) * width)
    v2 = Vector(math.cos(angle+math.pi/2) * height,
                math.sin(angle+math.pi/2) * height)

    p0 = origin
    p1 = p0.translate(v1)
    p2 = p0.translate(v1).translate(v2)
    p3 = p0.translate(v2)
    # print ("generate_random_rectangle", p0, p1, p2, p3)
    return Rectangle([p0, p1, p2, p3])


def float_equals(a, b):
    if abs(a-b) < epsilon:
        return True
    else:
        return False


def overlap(l0, l1):
    if min(l0) > max(l1):
        return False
    elif max(l0) < min(l1):
        return False
    else:
        return True


def is_min(v, l):
    if abs(v-min(l)) < epsilon:
        return True
    else:
        return False


def is_max(v, l):
    if abs(v-max(l)) < epsilon:
        return True
    else:
        return False


def main():
    print ("""Separating Axis Theorem
This is code to tell if 2 polygons are overlap or not
Copyright (C) 2020 Rafael Lee
This program is licensing under GPLv3
This program comes with ABSOLUTELY NO WARRANTY;
This is free software, and you are welcome to redistribute it
under certain conditions; https://www.gnu.org/licenses/""")

    print ("****************************************"*2)

    polygon_0_points = [(0, 2), (7, 2), (7, 8), (4, 3), (0, 8)]
    polygon0 = generate_polygon_from_point_list(polygon_0_points)
    is_convex, concave_points = polygon0.is_convex()
    if is_convex == True:
        print ("polygon0.is_convex() = True")
    else:
        print ("polygon0 is not convex, concave points = %s" %
               str([(lambda x:str(x))(x) for x in concave_points]))

    for i in range(10):
        plt.clf()

        polygon2 = generate_random_rectangle(width=10, height=1)
        print ("\ngenerate_random_rectangle() =", polygon2)
        print ("polygon2.is_convex() =", polygon2.is_convex())

        overlap = is_polygon_overlap(polygon0, polygon2)
        print("is_polygon_overlap (polygon0, polygon2) =", overlap)

        if overlap:
            plt_plot_polygon(polygon0, color='red', fill=True)
            plt_plot_polygon(polygon2, color='blue', fill=True)
        else:
            plt_plot_polygon(polygon0, color='red')
            plt_plot_polygon(polygon2, color='blue')

        plt.axes().set_aspect('equal')
        plt.show()


if __name__ == '__main__':
    main()
